#!/usr/bin/env python3

# -------------------------------
# projects/collatz/TestCollatz.py
# Copyright (C) 
# Glenn P. Downing
# -------------------------------

# https://docs.python.org/3.6/reference/simple_stmts.html#grammar-token-assert_stmt

# -------
# imports
# -------

from io import StringIO
from unittest import main, TestCase

from Collatz import collatz_read, collatz_eval, collatz_print, collatz_solve, calc_cycle


# -----------
# TestCollatz
# -----------


class TestCollatz(TestCase):

    # ---------------------------------------------------------------------------------------------------------
    #                                   My New Test Cases
    # ---------------------------------------------------------------------------------------------------------

    # Hasan

    # ----
    # read
    # ----

    def test_read1(self):
        s = "100 101\n"
        i, j = collatz_read(s)
        self.assertEqual(i, 100)
        self.assertEqual(j, 101)

    def test_read2(self):
        s = "4 24\n"
        i, j = collatz_read(s)
        self.assertEqual(i, 4)
        self.assertEqual(j, 24)

    def test_read3(self):
        s = "2 2\n"
        i, j = collatz_read(s)
        self.assertEqual(i, 2)
        self.assertEqual(j, 2)

    def test_read4(self):
        s = "9 999\n"
        i, j = collatz_read(s)
        self.assertEqual(i, 9)
        self.assertEqual(j, 999)

        # ----
        # eval
        # ----

    def test_eval_11(self):
        v = collatz_eval(10, 10)
        self.assertEqual(v, 7)

    ## corner case 1st number is grater than second num
    def test_eval_12(self):
        v = collatz_eval(100, 10)
        self.assertEqual(v, 1)

    # corner case -ve num
    def test_eval_14(self):
        v = collatz_eval(1, -10)
        self.assertEqual(v, 1)

    # -----
    # print
    # -----

    def test_print1(self):
        w = StringIO()
        collatz_print(w, 10, 100, 200)
        self.assertEqual(w.getvalue(), "10 100 200\n")

    def test_print2(self):
        w = StringIO()
        collatz_print(w, 9, 89, 99)
        self.assertEqual(w.getvalue(), "9 89 99\n")

    def test_print3(self):
        w = StringIO()
        collatz_print(w, 78, 82, 89)
        self.assertEqual(w.getvalue(), "78 82 89\n")

    def test_print4(self):
        w = StringIO()
        collatz_print(w, 23, 32, 33)
        self.assertEqual(w.getvalue(), "23 32 33\n")

    # -----
    # solve
    # -----
    def test_solve1(self):
        r = StringIO("10 20\n30 40\n50 60\n70 80\n")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(
            w.getvalue(), "10 20 21\n30 40 107\n50 60 113\n70 80 116\n")

    def test_solve2(self):
        r = StringIO("99 100\n76 77\n2 9\n3 80\n")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(
            w.getvalue(), "99 100 26\n76 77 23\n2 9 20\n3 80 116\n")

    def test_solve3(self):
        r = StringIO("3 5\n7 9\n11 13\n15 17\n")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(
            w.getvalue(), "3 5 8\n7 9 20\n11 13 15\n15 17 18\n")

    def test_solve4(self):
        r = StringIO("8 10\n12 14\n16 18\n20 22\n")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(
            w.getvalue(), "8 10 20\n12 14 18\n16 18 21\n20 22 16\n")

    def test_solve5(self):
        r = StringIO("9 10\n11 12\n13 14\n15 16\n")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(
            w.getvalue(), "9 10 20\n11 12 15\n13 14 18\n15 16 18\n")

    # -----
    # calc_cycle
    # -----

    def test_calc_cycle_1(self):
        v = calc_cycle(10)
        self.assertEqual(v, 7)

    def test_calc_cycle_2(self):
        v = calc_cycle(200)
        self.assertEqual(v, 27)

    def test_calc_cycle_3(self):
        v = calc_cycle(100)
        self.assertEqual(v, 26)

    def test_calc_cycle_4(self):
        v = calc_cycle(9)
        self.assertEqual(v, 20)

    # ---------------------------------------------------------------------------------------------------------
    #                                   Existing Test Cases fix
    # ---------------------------------------------------------------------------------------------------------

    # ----
    # read
    # ----

    def test_read(self):
        s = "1 10\n"
        i, j = collatz_read(s)
        self.assertEqual(i, 1)
        self.assertEqual(j, 10)

    # ----
    # eval
    # ----

    def test_eval_1(self):
        v = collatz_eval(1, 10)
        self.assertEqual(v, 20)

    def test_eval_2(self):
        v = collatz_eval(100, 200)
        self.assertEqual(v, 125)

    def test_eval_3(self):
        v = collatz_eval(201, 210)
        self.assertEqual(v, 89)

    def test_eval_4(self):
        v = collatz_eval(900, 1000)
        self.assertEqual(v, 174)

    # -----
    # print
    # -----

    def test_print(self):
        w = StringIO()
        collatz_print(w, 1, 10, 20)
        self.assertEqual(w.getvalue(), "1 10 20\n")

    # -----
    # solve
    # -----

    def test_solve(self):
        r = StringIO("1 10\n100 200\n201 210\n900 1000\n")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(
            w.getvalue(), "1 10 20\n100 200 125\n201 210 89\n900 1000 174\n")



# ----
# main
# ----

if __name__ == "__main__":
    main()

""" #pragma: no cover
$ coverage run --branch TestCollatz.py >  TestCollatz.out 2>&1


$ cat TestCollatz.out
.......
----------------------------------------------------------------------
Ran 7 tests in 0.000s
OK


$ coverage report -m                   >> TestCollatz.out



$ cat TestCollatz.out
...........................
----------------------------------------------------------------------
Ran 27 tests in 0.024s

OK
Name             Stmts   Miss Branch BrPart  Cover   Missing
------------------------------------------------------------
Collatz.py          25      0     10      0   100%
TestCollatz.py     114      0      0      0   100%
------------------------------------------------------------
TOTAL              139      0     10      0   100%
 
"""
